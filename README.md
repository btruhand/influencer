INFLUENCER
==========

A module to find out certain features of Twitter users used to determine later on their influence scores. By default and minially the module will inspect the last 200 statuses made by the twitter user and up to the last 28 days (can be changed by assigning a value to the per\_many\_days parameter when creating the object). This can be extended to at most 800 statuses (by assigning a value to the status\_retrieved parameter when creating the object). Either a Twitter ID or Twitter name must be given (they must be correct i.e the ID is the ID of the Twitter user with that Twitter name)

Usage
-----

An example is provided in collectdata.py. Invoke **python collectdata.py trainingset.txt** to see the result which would be saved to
**resultcollecting.txt**

Modules needed
--------------

Python-Twitter by Bear is needed. For installation of the module please go to [here](https://github.com/bear/python-twitter)

Important Notes
---------------

Do not delete the file **appInfo.py** since it is required in order for the module to work

Features Gathered
-----------------

* If follower is higher than following
* Ratio of following per follower
* The total number of retweets the user garners per day
* The ratio of posts that are retweeted
* The number of mentions made by the user which are in their following list
* The number of mentions made by the user which are not in their following list
* The ratio of mentions made per tweet
* The number of posts made which were favorited
* The total number of favorites garnered
* The ratio of favorited posts per tweet
* Whether the user is a Verified user or not

Important Functions and Files
-----------------------------

* Generally a you will only need to invoke RunCollector()
* A \_\_str\_\_ method is provided to give a formatted output of the object (used with *str()* or *print*) which includes the features' name, features' score and the Twitter name and ID of the Twitter user inspected
* FeatureScores() will return a single line of all the feature scores delimited by single space
* FeatureNames will return a single line of all the feature names delimited by single space
* **extractor.py** is needed to extract and create individual files for the feature names, feature values, and observed value based on gathered result in resultcollecting.txt. They are saved in the files named featurenames.txt features.txt and actualvalues.txt respectively once extractor.py is invoked

Important Notes
---------------

* trainingset.txt contains the Twitter names and their observed influence scores
* resultcollecting.txt contains the search result and the feature scores of 800 of the Twitter accounts specified in trainingset.txt 
* extractor.py should be invoked in the following format: __python extractor.py *search result file* *training set file* *number of features*__ thus it should currently be invoked as follows: __python extractor.py resultcollecting.txt trainingset.txt 12__
