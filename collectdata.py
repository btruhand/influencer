import collector
import sys
import os

def main():
    if len(sys.argv) == 1:
        sys.stderr.write("Have to specify a filename\n")
        return
    elif len(sys.argv) > 2:
        sys.stderr.write("Too many filenames specified\n")
        return
    else:
        with open(os.path.dirname(os.path.realpath(__file__))+'/'+'resultcollecting.txt', 'w') as write_file:
            with open(os.path.dirname(os.path.realpath(__file__))+'/'+sys.argv[1]) as f:
                counter = 0
                for twitter_user in f:
                    if counter == 800:
                        break
                    try:
                        twitter_name = twitter_user.split()[0]
                        user_collector = collector.Collector(twitter_name = twitter_name, per_many_days = 60.0, status_retrieved = 400)
                        user_collector.RunCollector()
                        write_file.write('{0} \n'.format(str(user_collector)))
                        counter+= 1
                    except collector.CollectError as exc:
                        sys.stderr.write("{0} \n".format(exc))
                    
main()
