import appInfo
import twitter
import time
import sys
import datetime
import math

class CollectError(Exception):
    pass

class Collector(object):

    api = twitter.Api(consumer_secret = appInfo.CONSUMER_SECRET,
            consumer_key = appInfo.CONSUMER_KEY,
            access_token_key = appInfo.ACCESS_TOKEN_KEY,
            access_token_secret = appInfo.ACCESS_TOKEN_SECRET)

    def __init__(self, twitter_id=None, twitter_name=None, per_many_days=None, status_retrieved = 200):
        if twitter_id or twitter_name:
            self.twitter_id = twitter_id
            self.twitter_name = twitter_name
        else:
            raise CollectError("Have to specify a twitter ID or twitter name")
        if per_many_days is not None:
            tweaked_per_many_days = math.floor(float(per_many_days))
            if tweaked_per_many_days != 0.0:
                self.per_many_days = math.floor(float(per_many_days))
            else:
                sys.stderr.write("Can't specifiy per_many_days to be zero\n")
                return
        else:
            self.per_many_days = 28.0   

        if status_retrieved < 200:
            self.status_retrieved = 200
            self.search_iteration = 1
        else:
            if status_retrieved > 800:
                status_retrieved = 800
            self.status_retrieved = status_retrieved
            self.search_iteration = self.status_retrieved / 200
    
        self.statuses = None
        self.following_list = None
        self.user = None
        self.day_difference = 0
        self.factor = 1.0
        self.ready = False
        self.furthest_post = 0
        self.follower_count = 0
        self.following_count = 0
        self.retweeted_posts = 0
        self.total_retweeted = 0.0
        self.total_tweets = 0.0
        self.all_features = dict()

        #The features
        self.follower_higher = 0
        self.following_follower_ratio = 0.0
        self.total_retweeted_per_day = 0.0
        self.retweeted_posts_per_tweet = 0.0 
        self.total_mentions_made_following = 0.0
        self.total_mentions_made_nonfollow = 0.0
        self.mentions_made_per_tweet = 0.0
        self.verified = 0
        self.favorited_posts = 0.0
        self.total_favorited = 0.0
        self.favorited_posts_per_tweet = 0.0
        self.total_favorited_per_tweet = 0.0
       # self.unique_retweeters = 0
       # self.unique_mentioned_following = 0
       # self.unique_mentioned_nonfollow = 0
       # self.unique_replied_following = 0
       # self.unique_replied_nonfollow =0
        
        sys.stderr.write("Made a Collector object with with screen name: {0} twitter ID: {1} and data gathered per: {2} days\n".format(self.twitter_name, self.twitter_id, self.per_many_days))

    def ReviewLimit(self, resource):
        try:
            sec = self.api.GetSleepTime(resource)
            if sec > 0:
                sys.stderr.write("Sleeping for 905 seconds for {0}\n".format(resource))
                time.sleep(905)
        except twitter.error.TwitterError as exc:
            sys.stderr.write("Error:{0} when trying to get sleep time\n".format(exc))
            sys.stderr.write("Rate limited, sleeping for 905 seconds when checking for a resource\n")
            time.sleep(905)

    def SetStatuses(self):
        try:
            counter = 0
            max_id = 0
            while counter != self.search_iteration:
                self.ReviewLimit('/statuses/user_timeline')
                if counter == 0:
                    self.statuses = self.api.GetUserTimeline(user_id=self.twitter_id, screen_name=self.twitter_name, count=200, include_rts=False)  
                    if not self.statuses:
                        break
                    max_id = self.statuses[-1].GetId() - 1
                else:
                    if counter == self.search_iteration - 1:
                        get_new_statuses = self.api.GetUserTimeline(user_id=self.twitter_id, screen_name=self.twitter_name, count=self.status_retrieved % 200, include_rts=False, max_id=max_id)
                    else:
                        get_new_statuses = self.api.GetUserTimeline(user_id=self.twitter_id, screen_name=self.twitter_name, count=200, include_rts=False, max_id=max_id)
                    if not get_new_statuses:
                        break
                    self.statuses+= get_new_statuses
                    max_id = self.statuses[-1].GetId()-1
                counter+=1
        except twitter.error.TwitterError as exc:
            raise CollectError("Encountered an error: {0} for twitter ID: {1} and twitter name: {2} when trying to get statuses".format(exc, self.twitter_id, self.twitter_name))

    def SetUser(self):
        self.ReviewLimit('/users/show/:id')
        try:
            if self.twitter_id is not None:
                self.user = self.api.GetUser(user_id = self.twitter_id)
            else:
                self.user = self.api.GetUser(screen_name = self.twitter_name)
        except twitter.error.TwitterError as exc:
            raise CollectError("Encountered an error: {0} for twitter ID: {1} when trying to get user information".format(exc, self.twitter_id))

    def SetFollowerCount(self):
        if self.follower_count is not None:
            self.follower_count = self.user.GetFollowersCount()
        else:
            raise CollectError("User has not been set")

    def SetFollowerFollowing(self):
        if self.follower_count is not None:
            if self.user is not None:
                self.following_count = self.user.GetFriendsCount()
                if self.following_count != 0:
                    if self.follower_count > self.following_count:
                        self.follower_higher = 1
                else:
                    self.follower_higher = 1

                #not 0
                if self.follower_count:
                    self.following_follower_ratio = self.following_count / float(self.follower_count)
                else:
                    self.following_follower_ratio = self.following_count
            else:
                raise CollectError("User has not been set")
        else:
            raise CollectError("Follower Count has not been set")
        self.all_features['FollowerHigher'] = self.follower_higher
        self.all_features['FollowingFollowerRatio'] = self.following_follower_ratio

    def SetFollowingList(self):
        try:
            self.ReviewLimit('/friends/ids')
            self.following_list = self.api.GetFriendIDs(user_id = self.twitter_id)
        except twitter.error.TwitterError as exc:
            raise CollectError("Encountered an error: {0} for twitter ID: {1} when trying to gather following list\n".format(exc, self.twitter_id))

    def BasicStatusesReview(self):
        if self.statuses is not None:
            most_recent = None
            for status in self.statuses: 
                string_date = status.GetCreatedAt()
                if most_recent is None:
                    most_recent = datetime.datetime.strptime(string_date, "%a %b %d %H:%M:%S +0000 %Y")

                current_status_date = datetime.datetime.strptime(string_date, "%a %b %d %H:%M:%S +0000 %Y")
                date_difference = most_recent - current_status_date
                if date_difference.days < self.per_many_days:
                    self.day_difference = date_difference.days
                    self.furthest_post+= 1
                    self.total_tweets+= 1
                else:
                    self.day_difference = date_difference.days
                    break
            if self.day_difference < self.per_many_days:
                self.factor = self.per_many_days/(1+self.day_difference)
                self.total_tweets = math.floor(self.total_tweets * self.factor)

        else:
            raise CollectError("Statuses has not been set")
            
    def SetVerified(self):
        if self.user is not None:
            if self.user.GetVerified():
                self.verified = 1
            else:
                self.verified = 0
            
            self.all_features['Verified'] = self.verified
        else:
            raise CollectError("User has not been set")
   
    def ReviewRetweets(self):
        if self.statuses is not None:
            post_counter = 0
            for status in self.statuses:
                if post_counter <= self.furthest_post:
                    post_counter+= 1
                    number_of_retweets = status.GetRetweetCount()
                    if number_of_retweets > 0:
                        self.retweeted_posts+= 1
                        self.total_retweeted+= number_of_retweets
                else:
                    break
            
            if self.day_difference < self.per_many_days:
                self.retweeted_posts = math.floor(self.retweeted_posts * self.factor)
                self.total_retweeted = math.floor(self.total_retweeted * self.factor)
          
            self.total_retweeted_per_day = self.total_retweeted/self.per_many_days
            
            if int(self.total_tweets) != 0:
                self.retweeted_posts_per_tweet = self.retweeted_posts/self.total_tweets
            
            self.all_features['TotalRetweetedPerDay'] = self.total_retweeted_per_day
            self.all_features['RetweetedPostsPerTweet'] = self.retweeted_posts_per_tweet
        else:
            raise CollectError("Statuses has not been set")

    def ReviewMentions(self):
        if self.statuses is not None:
            if self.following_list is not None:
                post_counter = 0
                for mention in self.statuses:
                    if post_counter <= self.furthest_post:
                        post_counter+= 1
                        mentioned_users = mention.user_mentions
                        for mentioned in mentioned_users:
                            if mentioned.GetId() in self.following_list:
                                self.total_mentions_made_following+= 1
                            else:
                                self.total_mentions_made_nonfollow+= 1
                    else:
                        break

                if self.day_difference < self.per_many_days:
                    self.total_mentions_made_following = math.floor(self.total_mentions_made_following * self.factor)
                    self.total_mentions_made_nonfollow = math.floor(self.total_mentions_made_nonfollow * self.factor)

                float_mentions = self.total_mentions_made_following + self.total_mentions_made_nonfollow
                if int(self.total_tweets) != 0:
                    self.mentions_made_per_tweet = float_mentions/self.total_tweets 

                self.all_features['TotalMentionFollowing'] = self.total_mentions_made_following
                self.all_features['TotalMentionNonfollow'] = self.total_mentions_made_nonfollow
                self.all_features['MentionedPerTweet'] = self.mentions_made_per_tweet
            else:
                raise CollectError("Following list has not been set")
        else:
            raise CollectError("Statuses have not been set")

    def ReviewFavorites(self):
        if self.statuses is not None:
            post_counter = 0
            for status in self.statuses:
                if post_counter <= self.furthest_post:
                    number_of_favorites = status.GetFavoriteCount()
                    if number_of_favorites > 0:
                        self.favorited_posts+= 1
                        self.total_favorited+= number_of_favorites
                else:
                    break
                post_counter+=1

            if self.day_difference < self.per_many_days:
                self.favorited_posts = math.floor(self.favorited_posts * self.factor)
                self.total_favorited = math.floor(self.total_favorited * self.factor)
            
            if int(self.total_tweets) != 0:
                self.favorited_posts_per_tweet = self.favorited_posts/self.total_tweets
                self.total_favorited_per_tweet = self.total_favorited/self.total_tweets

            self.all_features['FavoritedPosts'] = self.favorited_posts
            self.all_features['TotalFavorted'] = self.total_favorited
            self.all_features['FavoritedPostsRatio'] = self.favorited_posts_per_tweet
            self.all_features['TotalFavoritedPerTweet'] = self.total_favorited_per_tweet
        else:
            raise CollectError("Statuses have not been set")

    def SetEverything(self):
        """Needs to be invoked before data collection can happen. An alternative is to manually invoke these"""
        self.SetStatuses() 
        self.SetUser()
        self.SetFollowingList()
        self.SetFollowerCount()
        self.SetFollowerFollowing()
        self.SetVerified()
        self.BasicStatusesReview()

    def __str__(self):
        """Gives a formatted layout of the object. The format is simply *Feature name*:*Value* for each feature in the model"""
        res = ''
        first = True
        for feature in self.all_features:
            if first:
                res+= '{}'.format(self.all_features[feature])
                first = False
            else:
                res+= ' {}'.format(self.all_features[feature])
        return res

    def RunCollector(self):
        """Run the whole data collection correctly"""
        self.SetEverything()
        self.ReviewRetweets()
        self.ReviewMentions()
        self.ReviewFavorites()

    def __str__(self):
        res = ''
        first = True
        for feature in self.all_features:
            if first:
                res+= '{}:{}'.format(feature,self.all_features[feature])    
                first = False
            else:
                res+= ' {}:{}'.format(feature,self.all_features[feature])
        return res+' TwitterName:{} TwitterID:{}'.format(self.twitter_name, self.twitter_id)

    def FeatureScores(self):
        res = ''
        first = True
        for feature in self.all_features:
            if first:
                res+= '{}'.format(self.all_features[feature])
                first = False
            else:
                res+= ' {}'.format(self.all_features[feature])
        return res

    def FeatureNames(self):
        res = ''
        first = True
        for feature in self.all_features:
            if first:
                res+= feature
                first = False
            else:
                res+= ' '+feature
        return
